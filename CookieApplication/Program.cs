﻿using CookieApplication.Cookies;
using CookieApplication.Kitchen;
using System;
using System.Collections.Generic;


namespace CookieApplication
{
    class Program
    {
        static ICookieJar jar = new CookieJar();
        static ICookieMaker maker = new CookieMaker();
        static ICookieEater eater = new CookieEater();
        static void Main(string[] args)
        {
            //Cookies.AddRange(maker.MakeCookies()); //AddRange => permite adaugarea unei liste != Add
            jar.AddCookies(maker.MakeCookies());

            Console.WriteLine("....");
           

            Console.ReadKey();
        }

       
    }
}
