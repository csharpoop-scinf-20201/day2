﻿using CookieApplication.Cookies;
using System;
using System.Collections.Generic;
using System.Text;

namespace CookieApplication.Kitchen
{
    class CookieJar : ICookieJar
    {
        private List<Cookie> myCookies = new List<Cookie>();

        public void AddCookies(List<Cookie> cookies)
        {   
            //Option A 
            //myCookies.AddRange(cookies);

            //Option B
            cookies.ForEach((item) => AddCookies(item));
        }

        public List<Cookie> GetCookies()
        {
            return myCookies;
        }

        private void AddCookies(Cookie item)
        {
            myCookies.Add(item);
        }
    }
}
