﻿using CookieApplication.Cookies;
using System;
using System.Collections.Generic;
using System.Text;

namespace CookieApplication.Kitchen
{
    interface ICookieJar
    {
        void AddCookies(List<Cookie> cookies);

        List<Cookie> GetCookies();
    }
}
