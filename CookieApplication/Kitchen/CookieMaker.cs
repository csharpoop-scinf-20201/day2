﻿using CookieApplication.Cookies;
using System;
using System.Collections.Generic;
using System.Text;

namespace CookieApplication.Kitchen
{
    class CookieMaker : ICookieMaker
    {
        public string DefaultFlavour = "vanilla";
        public List<Cookie> MakeCookies()
        {
            List<Cookie> CookieList = new List<Cookie>();
            Cookie cookie1 = new Cookie();
            cookie1.IsSweet = true;

            Cookie cookie2 = new Cookie();
            cookie2.IsSweet = false;

            FlavouredCookie vanillaCookie = new FlavouredCookie();
            vanillaCookie.Flavour = "vanilla";

            DualFlavourCookie dualCookie = new DualFlavourCookie();
            dualCookie.Flavour = "vanilla";
            dualCookie.SecondFlavour = "chocolate";

            CookieList.Add(cookie1);
            CookieList.Add(cookie2);
            CookieList.Add(vanillaCookie);

            return CookieList;
        }
    }
}
