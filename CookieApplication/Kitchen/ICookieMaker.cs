﻿using CookieApplication.Cookies;
using System;
using System.Collections.Generic;
using System.Text;

namespace CookieApplication.Kitchen
{
    interface ICookieMaker
    {
        List<Cookie> MakeCookies();
    }
}
