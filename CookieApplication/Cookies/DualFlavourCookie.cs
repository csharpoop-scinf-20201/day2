﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CookieApplication.Cookies
{
    class DualFlavourCookie : FlavouredCookie
    {
        public string SecondFlavour;

        public string GetDescription()
        {
            return $"I am a {(IsSweet ? "sweet" : "not sweet")} cookie of flavours: {Flavour}, {SecondFlavour}";
        }

        public override string GetBetterDescription()
        {
            return $"I am a {(IsSweet ? "sweet" : "non sweet")} cookie of flavours: {Flavour}, {SecondFlavour}";
        }
    }
}
