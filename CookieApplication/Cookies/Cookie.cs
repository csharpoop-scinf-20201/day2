﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CookieApplication.Cookies
{
    class Cookie
    {
        public bool IsSweet;

        public int Weight;

        public string GetDescription()
        {
            return $"I am a {(IsSweet ? "sweet" : "non sweet")} cookie";
        }

        public virtual string GetBetterDescription()
        {
            return $"I am a {(IsSweet ? "sweet" : "non sweet")} cookie";
        }
    }
}
