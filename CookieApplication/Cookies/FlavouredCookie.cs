﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CookieApplication.Cookies
{
    class FlavouredCookie : Cookie
    {
        public string Flavour;

        public string GetDescription()
        {
            return $"I am a {(IsSweet ? "sweet" : "non sweet")} cookie";
        }

        public override string GetBetterDescription()
        {
            return $"I am a {(IsSweet ? "sweet" : "non sweet")} cookie";
        }
    }

   
}
